import {LitElement, html, css} from 'lit-element'

class TestBootstrap extends LitElement {

    static get properties(){
        return {

        }
    }

    constructor() {
        super()
    }
    
static get styles() {
    return css`
        .redgb {
            background-color: red
        }
        .greengb {
            background-color: green
        }
        .bluebg {
            background-color: blue
        }
        .greybg { 
            background-color: grey
        }
    `
}

    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">    
            <h3>Test Bootstrap</h3>
            <div class="row greybg">
                <div class="col-2 offset-1 sm-6 redgb">Col 1</div>
                <div class="col-3 offset-2 sm-1 bluebg">Col 2</div>
                <div class="col-4 sm-1 greengb">Col 3</div>
            </div>
        `
    }
}

customElements.define('test-bootstrap', TestBootstrap)